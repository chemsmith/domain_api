# DomainAPI

A simple python package for accessing the public API provided by [domain.com.au](https://developer.domain.com.au/docs/introduction).
Currently this package only wraps the API endpoints accessible using the "Client Credentials" authentication type.

### Prerequisites

* Python 3
* Requests

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Dylan Smith** - *Initial work*

See also the list of [contributors](https://gitlab.com/chemsmith/domain_api/graphs/master) who participated in this project.

## License

This project is licensed under the GNU GPLv3 License - see the [LICENSE](LICENSE) file for details
