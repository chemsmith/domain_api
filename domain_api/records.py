import requests

if False:
    from . import DomainAPI


class DomainAPIRecords:
    def fetch_records(self: DomainAPI, url, request_payload, required_scope = None):
        total_records = None
        request_payload['page'] = 1
        request_payload['pageSize'] = 100
        listings = []
        if not self.valid_credentials(required_scope):
            try:
                self.get_client_credentials_grant(required_scope)
            except Exception as e:  # TODO should catch inability to access required credentials
                print(str(e))
                return None

        while total_records is None or 100 * request_payload['page'] < total_records:
            p = requests.post(url, json=request_payload,
                              headers={"Authorization": "Bearer " + self.access_token})

            if p.status_code == 500:
                raise self.HTTPError()

            if total_records is None:
                total_records = int(p.headers['X-Total-Count'])

            if total_records == 0:
                return []

            listings.extend(p.json())

            request_payload['page'] += 1

            self.refresh_client_credentials_grant()

        if len(listings) != total_records:
            raise self.MismatchingRecordCount()

        return listings

    # Exceptions
    class HTTPError(Exception):
        pass

    class MismatchingRecordCount(Exception):
        def __init__(self):
            super().__init__("A valid access token could not be retrieved. Check the client ID and secret.")
