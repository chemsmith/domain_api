from .authentication import DomainAPIAuthentication
from .listings import DomainAPIListings
from .records import DomainAPIRecords


class DomainAPI(object, DomainAPIAuthentication, DomainAPIListings, DomainAPIRecords):
    def __init__(self, client_id, client_secret):
        self.client_token = None  # dict('token': , 'expiry': , 'scope': )
        self.client_id = client_id
        self.secret = client_secret
